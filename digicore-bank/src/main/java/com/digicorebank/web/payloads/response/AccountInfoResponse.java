package com.digicorebank.web.payloads.response;

import com.digicorebank.data.model.Account;
import lombok.Data;

@Data
public class AccountInfoResponse {
    private int responseCode;
    private boolean success;
    private String message;
    private Account account;
}
