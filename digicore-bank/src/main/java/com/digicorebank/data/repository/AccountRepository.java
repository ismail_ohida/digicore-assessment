package com.digicorebank.data.repository;

import com.digicorebank.data.model.Account;
import com.digicorebank.data.model.Role;
import com.digicorebank.web.payloads.request.CreateAccountRequest;
import org.apache.commons.text.CharacterPredicates;
import org.apache.commons.text.RandomStringGenerator;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository(value = "accountRepository")
public class AccountRepository {
    private final Map<String, Account> accounts = new HashMap<>();

    private String generateAccountNumber() {
        RandomStringGenerator randomStringGenerator = new RandomStringGenerator.Builder()
                .withinRange('0', 'z')
                .filteredBy(CharacterPredicates.LETTERS, CharacterPredicates.DIGITS)
                .build();
        return randomStringGenerator.generate(10);
    }


    public Account createAccount(CreateAccountRequest request) {
        Account account = new Account();
        account.setAccountName(request.getAccountName());
        account.setBalance(request.getInitialDeposit());
        account.setAccountNumber(generateAccountNumber());
        account.setAccountPassword(request.getAccountPassword());
        account.addRole(Role.CUSTOMER);
        accounts.put(account.getAccountNumber(), account);
        return account;
    }


    public Account getAccountByAccountName(String accountName) {
        return accounts.values().stream().filter(account -> account.getAccountName().equalsIgnoreCase(accountName)).findFirst().get();
    }

    public void clearDatabase() {
        accounts.clear();
    }

    public boolean accountNameExists(String accountName) {
        return accounts.values().stream().anyMatch(
                account -> account.getAccountName().equalsIgnoreCase(accountName));
    }

    public Account getAccountByAccountNumber(String accountNumber) {
        return accounts.get(accountNumber);
    }

    public void updateAccount(String accountNumber, Account accountToFund) {
        accounts.put(accountNumber, accountToFund);
    }
}
