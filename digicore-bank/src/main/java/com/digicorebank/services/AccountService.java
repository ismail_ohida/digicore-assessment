package com.digicorebank.services;

import com.digicorebank.data.model.Account;
import com.digicorebank.data.model.Transaction;
import com.digicorebank.web.payloads.request.AccountDetailsRequest;
import com.digicorebank.web.payloads.request.CreateAccountRequest;
import com.digicorebank.web.payloads.request.DepositRequest;
import com.digicorebank.web.payloads.request.WithdrawRequest;

import java.util.List;

public interface AccountService {
    void createAccount(CreateAccountRequest request);
    String deposit(DepositRequest request);
    String withdraw(WithdrawRequest request);
    List<Transaction> getAccountStatement(AccountDetailsRequest request, String accountNumber);
    Account getAccountInfo(AccountDetailsRequest request, String accountNumber);
}
