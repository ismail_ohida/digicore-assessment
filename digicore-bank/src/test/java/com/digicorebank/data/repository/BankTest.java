package com.digicorebank.data.repository;

import com.digicorebank.data.model.Account;
import com.digicorebank.data.model.Transaction;
import com.digicorebank.data.model.TransactionType;
import com.digicorebank.services.AccountService;
import com.digicorebank.services.AccountServiceImpl;
import com.digicorebank.web.exceptions.AccountException;
import com.digicorebank.web.payloads.request.AccountDetailsRequest;
import com.digicorebank.web.payloads.request.CreateAccountRequest;
import com.digicorebank.web.payloads.request.DepositRequest;
import com.digicorebank.web.payloads.request.WithdrawRequest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@Slf4j
class BankTest {

    @Autowired
    AccountService accountService;
    @Autowired
    AccountRepository accountRepository;

    Account demoAccount;
    @BeforeEach
    void setUp() {
        CreateAccountRequest request = new CreateAccountRequest();
        request.setAccountName("demo");
        request.setInitialDeposit(10000.00);
        request.setAccountPassword("test");
        accountService.createAccount(request);
        demoAccount = accountRepository.getAccountByAccountName("demo");
    }
    @AfterEach
    void tearDown() {
        accountRepository.clearDatabase();
    }

    @Test
    void createAccount() {
        CreateAccountRequest request = new CreateAccountRequest();
        request.setAccountName("Abdul");
        request.setInitialDeposit(501.00);
        request.setAccountPassword("test");
        accountService.createAccount(request);
        Account createdAccount = accountRepository.getAccountByAccountName("Mofe");
        assertNotNull(createdAccount);
        assertEquals(501.00, createdAccount.getBalance());
        assertNotEquals("test", createdAccount.getAccountPassword());
    }

    @Test
    void createAccountThrowsExceptionIfNameAlreadyExists() {
        CreateAccountRequest request = new CreateAccountRequest();
        request.setAccountName("demo");
        request.setInitialDeposit(501.00);
        request.setAccountPassword("test");
        assertThrows(AccountException.class, ()-> accountService.createAccount(request));
    }

    @Test
    void deposit() {
        DepositRequest request = new DepositRequest();
        request.setAccountNumber(demoAccount.getAccountNumber());
        request.setAmount(1000.00);
        accountService.deposit(request);
        Account updatedAccount = accountRepository.getAccountByAccountName("demo");
        assertEquals(11000.00, updatedAccount.getBalance());
    }

    @Test
    void depositThrowsAnAccountExceptionIfDepositAmountIsOverOrBelowThreshold() {
        DepositRequest request = new DepositRequest();
        request.setAccountNumber(demoAccount.getAccountNumber());
        request.setAmount(-1000.00);
        assertThrows(AccountException.class, ()-> accountService.deposit(request));
        request.setAmount(1_000_000_000.00);
        assertThrows(AccountException.class, ()-> accountService.deposit(request));
    }

    @Test
    void depositThrowsAnAccountExceptionIfWrongAccountNumberIsProvided(){
        DepositRequest request = new DepositRequest();
        request.setAccountNumber("wrongAccountNumber");
        request.setAmount(-1000.00);
        assertThrows(AccountException.class, ()-> accountService.deposit(request));
    }

    @Test
    void withdraw() {
        WithdrawRequest request = new WithdrawRequest();
        request.setAccountNumber(demoAccount.getAccountNumber());
        request.setWithdrawalAmount(1000.00);
        request.setAccountPassword("test");
        accountService.withdraw(request);
        Account updatedAccount = accountRepository.getAccountByAccountName("demo");
        assertEquals(9000.00, updatedAccount.getBalance());
    }

    @Test
    void withdrawThrowsAnAccountExceptionIfAccountPasswordIsIncorrect() {
        WithdrawRequest request = new WithdrawRequest();
        request.setAccountNumber(demoAccount.getAccountNumber());
        request.setWithdrawalAmount(600.00);
        request.setAccountPassword("testing");
        assertThrows(AccountException.class, ()-> accountService.withdraw(request));
    }


    @Test
    void getAccountStatement() {
        DepositRequest request = new DepositRequest();
        request.setAccountNumber(demoAccount.getAccountNumber());
        request.setAmount(1000.00);
        accountService.deposit(request);
        WithdrawRequest request2 = new WithdrawRequest();
        request2.setAccountNumber(demoAccount.getAccountNumber());
        request2.setWithdrawalAmount(1000.00);
        request2.setAccountPassword("test");
        accountService.withdraw(request2);
        AccountDetailsRequest detailsRequest = new AccountDetailsRequest();
        detailsRequest.setAccountPassword("test");
        List<Transaction> statement = accountService.getAccountStatement(detailsRequest,demoAccount.getAccountNumber());
        assertNotNull(statement);
        assertEquals(2, statement.size());
        assertEquals(TransactionType.DEPOSIT,statement.get(0).getTransactionType());
    }

    @Test
    void getAccountStatementThrowsAccountExceptionIfIncorrectPasswordOrIncorrectAccountGiven() {
        DepositRequest request = new DepositRequest();
        request.setAccountNumber(demoAccount.getAccountNumber());
        request.setAmount(1000.00);
        accountService.deposit(request);
        AccountDetailsRequest detailsRequest = new AccountDetailsRequest();
        detailsRequest.setAccountPassword("testing");
        assertThrows(AccountException.class, ()-> accountService.getAccountStatement(detailsRequest,demoAccount.getAccountNumber()));
        detailsRequest.setAccountPassword("test");
        assertThrows(AccountException.class, ()-> accountService.getAccountStatement(detailsRequest,"wrongAccountNumber"));
    }


    @Test
    void getAccountInfo() {
        DepositRequest request = new DepositRequest();
        request.setAccountNumber(demoAccount.getAccountNumber());
        request.setAmount(1000.00);
        accountService.deposit(request);
        AccountDetailsRequest detailsRequest = new AccountDetailsRequest();
        detailsRequest.setAccountPassword("test");
        Account account = accountService.getAccountInfo(detailsRequest, demoAccount.getAccountNumber());
        assertNotNull(account);
        assertEquals("demo", account.getAccountName());
        assertEquals(11000.00, account.getBalance());
    }

    @Test
    void getAccountInfoThrowsAccountExceptionIfWrongPasswordOrAccountNumberIsGiven() {
        DepositRequest request = new DepositRequest();
        request.setAccountNumber(demoAccount.getAccountNumber());
        request.setAmount(1000.00);
        accountService.deposit(request);
        AccountDetailsRequest detailsRequest = new AccountDetailsRequest();
        detailsRequest.setAccountPassword("testing");
        assertThrows(AccountException.class, ()-> accountService.getAccountInfo(detailsRequest, demoAccount.getAccountNumber()));
        detailsRequest.setAccountPassword("test");
        assertThrows(AccountException.class, ()-> accountService.getAccountInfo(detailsRequest, "wrongAccountNumber"));
    }

    @Test
    void loadAccountByAccountNumber(){
        AccountServiceImpl accountService = new AccountServiceImpl(accountRepository);
        UserDetails accountDetail = accountService.loadUserByUsername(demoAccount.getAccountNumber());
        log.info("Account detail ==> {}", (accountDetail));
        assertAll(
                () -> assertNotNull(accountDetail.getAuthorities()),
                () -> assertNotNull(accountDetail.getPassword())
        );
    }
}