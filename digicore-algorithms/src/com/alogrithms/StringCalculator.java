package com.alogrithms;

import java.util.Arrays;

public class StringCalculator {
    public static int calculator(String val1, String val2, String operator) {
        int[] comparison = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        if (val1.length() < 1 && val2.length() < 1 && !checkOperator(operator.charAt(0))) {
            return 0;
        }
        int total;
        int decVal1 = 0;
        int decVal2 = 0;

        decVal1 = getDecimalValues(val1, comparison, decVal1);
        decVal2 = getDecimalValues(val2, comparison, decVal2);
        total = applyOp(operator.charAt(0), decVal1, decVal2);
        return total;
    }

    private static int getDecimalValues(String val, int[] comparison, int decVal) {
        if (isGreaterThanOne(val)) {
            String[] characters = val.split("");
            int tempVal = 0;

            for (int index = 0; index < characters.length; index++) {
                for (int i : comparison) {
                    if (String.valueOf(i).equals(characters[index])) {
                        tempVal += (i * (int) Math.pow(10, (characters.length - 1) - index));
                        break;
                    }
                }
            }
            decVal = tempVal;
        } else {
            for (int i : comparison) {
                if (String.valueOf(i).equals(val)){
                    decVal = i;
                    break;
                }
            }
        }
        return decVal;
    }

    private static boolean isGreaterThanOne(String val1) {
        return val1.length() > 1;
    }

    public static int applyOp(char op, int a, int b) {
        switch (op) {
            case '+':
                return a + b;
            case '-':
                return a - b;
            case '*':
                return a * b;
            case '/':
                if (b == 0) throw new UnsupportedOperationException("Cannot divide by zero");
                return a / b;
        }
        return 0;
    }

    private static boolean checkOperator(Character operator) {
        boolean isValid = false;
        switch (operator) {
            case '+', '-', '/', '*' -> isValid = true;
        }
        return isValid;
    }
}
