package com.digicorebank.data.model;

public enum TransactionType {
    WITHDRAWAL,
    DEPOSIT
}
